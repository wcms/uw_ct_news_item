<?php

/**
 * @file
 * uw_ct_news_item.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_news_item_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'news_item-front_page';
  $context->description = 'Displays news item block on a site\'s front page.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_news_item-front_page' => array(
          'module' => 'uw_ct_news_item',
          'delta' => 'front_page',
          'region' => 'content',
          'weight' => '8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays news item block on a site\'s front page.');
  $export['news_item-front_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'news_item_categories';
  $context->description = 'Displays news item categories (taxonomy) blocks.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'news' => 'news',
        'news/*' => 'news/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_news_item-news_public_feed' => array(
          'module' => 'uw_ct_news_item',
          'delta' => 'news_public_feed',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'uw_ct_news_item-news_by_audience' => array(
          'module' => 'uw_ct_news_item',
          'delta' => 'news_by_audience',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'uw_ct_news_item-news_by_date' => array(
          'module' => 'uw_ct_news_item',
          'delta' => 'news_by_date',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays news item categories (taxonomy) blocks.');
  $export['news_item_categories'] = $context;

  return $export;
}
