<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <div class="news-item-header">
      <?php if (!$page && !$teaser): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>

    </div>

    <div class="content_node"<?php print $content_attributes; ?>>
    
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);

        if ($teaser) {
          print render($content['field_news_date']);
          print '<h2' . $title_attributes . '><a href="' . $node_url . '">' . $title . '</a></h2>';
          // Filter and trim the teaser content.
          $content['body'][0]['#markup'] = uw_ct_web_page_filter_summary($content['body']['#items'][0]);
          $content['body'][0]['#markup'] = preg_replace("/<img[^>]+\>/i", "", $content['body'][0]['#markup']);
          hide($content['field_news_date']);
          hide($content['title_field']);
        }
        hide($content['service_links']);
        print render($content);
       ?>
    </div>
    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>
    <?php if (!empty($content['field_tag'])): ?>
      <?php print render($content['field_tag']); ?>
    <?php endif; ?>
    <?php print render($content['service_links']); ?>
  </div> <!-- /node-inner -->
</div> <!-- /node-->
<?php print render($content['comments']); ?>
<?php if (!empty($content['links'])): ?>
  <div class="links"><?php print render($content['links']); ?></div>
<?php endif; ?>
