<?php

/**
 * @file
 * Default view template to display a item in an RSS feed.
 *
 * @ingroup views_templates
 *
 * Un-do double-escaping of titles.
 */
?>
  <item>
    <title><?php print html_entity_decode($title); ?></title>
    <link><?php print $link; ?></link>
    <description><?php print $description; ?></description>
    <?php print $item_elements; ?>
  </item>
